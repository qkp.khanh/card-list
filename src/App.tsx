import React, { Suspense } from "react";
import Loading from "./components/Loading";

const TripleList = React.lazy(() => import("./pages/TripleList"));

function App() {
  return (
    <Suspense fallback={<Loading/>}>
     <TripleList/>
    </Suspense>
  );
}

export default App;