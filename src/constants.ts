export const CARD_TYPE = {
    USER: "users",
    PRODUCT: "products",
    CART: "carts",
}

export const IMAGE_BY_CARD_TYPE: Record<string, string> = {
    users: "image",
    products: "thumbnail",
}

/* eslint-disable @typescript-eslint/no-explicit-any */
export type SearchingNameType = {
    name: string;
    signalControl: any;

}

export type ListFetchType = {
    page: number;
    signalControl: any;
}

export type CartFetchType = {
    userId: number;
    signalControl: any;
}

export const REQUEST_TIMEOUT = 5000;

export const ERROR_CODE_TO_MESSAGE: Record<string | number, string> = {
    UNKNOWN: "Something went wrong",
    ERR_NETWORK: "Connection was troubled",
    ECONNABORTED: "The request has been expired",
    404: "We couldn't find what you are looking for!",
    429: "Too many requests. Please try again later",
    500: "Internal Sever Error",
} 