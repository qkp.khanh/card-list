/* eslint-disable @typescript-eslint/no-explicit-any */
import axios from "axios";
import { ListFetchType, SearchingNameType } from "../constants";
import { getUserList, getUsersBySearching } from "../services/Users";
import { fetchImage, findIntersectionBetweenTwoArrayOfObjects } from "../utils/helper";

const searchingUsersByName = async (info: SearchingNameType) => {
  const { name, signalControl } = info;
  const responseList: any = [];
  let searchResult: any[];
  const nameList = name.split(" ");
  await Promise.all(
    nameList.map(async (element: string) => {
      const { data: { users }, status } = await getUsersBySearching({ name: element, signalControl: signalControl });
      status === 200 && responseList.push(users);
    })
  );
  // maximum 2 lists for response list - 1 for first name and 1 for last name
  if (responseList.length === 2) {
    searchResult = findIntersectionBetweenTwoArrayOfObjects(responseList);
  } else {
    searchResult = responseList[0];
  }
  return searchResult;
}

const fetchUsers = async(info:ListFetchType) => {
  const {
    data: { total, users: userList },
  } = await getUserList({ ...info });
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const listData = await axios.all(
      userList.map(async (element: any) => {
        const imageURL = await fetchImage(element.image);
        return { ...element, image: imageURL };
      })
    );
    return { listData, total };
}

export {searchingUsersByName, fetchUsers}