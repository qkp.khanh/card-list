import { CartFetchType } from "../constants";
import getCartByUser from "../services/Carts/getCartByUser";

const CartsFetcher = async(info: CartFetchType) => {
    const {data: {carts}, status} = await getCartByUser({...info});
    if (status === 200){
        return carts[0];
    }
}

export {CartsFetcher}