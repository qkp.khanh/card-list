/* eslint-disable @typescript-eslint/no-explicit-any */
import axios from "axios";
import { ListFetchType, SearchingNameType } from "../constants";
import { getProductList, getProductsBySearching } from "../services/Products";
import { fetchImage } from "../utils/helper";

const searchingProductsByName = async (info: SearchingNameType) => {
  const {
    data: { products },
  } = await getProductsBySearching({ ...info });
  return products;
};

const fetchProducts = async (info: ListFetchType) => {
  const {
    data: { total, products: productList },
  } = await getProductList({ ...info });
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const listData = await axios.all(
      productList.map(async (element: any) => {
        const imageURL = await fetchImage(element.thumbnail);
        return { ...element, thumbnail: imageURL };
      })
    );
    return { listData, total };
};

export { searchingProductsByName, fetchProducts };
