import ErrorPage from "./Error";
import Loading from "./Loading";
import { Spinner } from "./Spinner";
export { default as CloseIcon } from './CloseIcon';
export {ErrorPage, Loading, Spinner}