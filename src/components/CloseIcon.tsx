const CloseIcon = ({onClick}: {onClick?: () => void}) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      className="cursor-pointer"
      height="24"
      viewBox="0 -960 960 960"
      width="24"
      onClick={onClick}
    >
      <path d="m256-200-56-56 224-224-224-224 56-56 224 224 224-224 56 56-224 224 224 224-56 56-224-224-224 224Z" />
    </svg>
  );
};
export default CloseIcon;
