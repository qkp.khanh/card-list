/* eslint-disable @typescript-eslint/no-unused-vars */
import axios from "axios";
import { ERROR_CODE_TO_MESSAGE } from "../constants";

/* eslint-disable @typescript-eslint/no-explicit-any */
export const parseQueryToString = (queryString: string) => {
  if (!queryString) return {};
  const query = queryString.substring(1);
  const vars = query.split("&");
  const data: any = {};
  for (let i = 0; i < vars.length; i++) {
    const pair = vars[i].replace(/=/, "&").split("&");
    const key = decodeURIComponent(pair[0]);
    const value = decodeURIComponent(pair[1] || "");
    if (key.length > 0) {
      data[key] = value;
    }
  }
  return data;
};

export const objectToQueryString = (object: any) => {
  const obj = { ...object };
  Object.keys(obj).forEach(key => {
    if (!obj[key] && obj[key] !== false) delete obj[key];
  });
  return Object.keys(obj)
    .map(key => {
      if (Array.isArray(obj[key])) {
        let str = "";
        obj[key].forEach((value: any, idx: number, array: string | any[]) => {
          if (idx === array.length - 1) {
            str += `${key}[]=${value}`;
          } else {
            str += `${key}[]=${value}` + "&";
          }
        });
        return str;
      }
      return `${encodeURIComponent(key)}=${encodeURIComponent(obj[key])}`;
    })
    .join("&");
};

export const fetchImage = async (url:string) => {
  const {data : imageBlob} = await axios.get(url, {responseType: 'blob'});
  const imageObjectURL = URL.createObjectURL(imageBlob);
  return imageObjectURL;
};



export const findIntersectionBetweenTwoArrayOfObjects = (array:any) => {
  const idList:number[] = [];
  const result:any[] = [];
  array.map((response:any) => {
    response.forEach((user:any) => {
      if(idList.includes(user.id)){
        result.push(user);
      }else {idList.push(user.id)}
    });
  })
  return result;
}

export const errorHandler = (error:any) => {
    //here we have a type guard check, error inside this if will be treated as AxiosError
    if (error?.response) {
      const statusCode = error.response.status;
      return Error(ERROR_CODE_TO_MESSAGE[statusCode])
    }
    if (error?.code){
      return Error(ERROR_CODE_TO_MESSAGE[error.code])
    }
    return Error(error?.message);
}