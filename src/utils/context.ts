/* eslint-disable @typescript-eslint/no-explicit-any */
import { createContext } from "react";

export const SelectedContext = createContext<any>(null);
