/* eslint-disable @typescript-eslint/no-explicit-any */

import { useState } from "react";
import UserCardList from "./components/User/List";
import { SelectedContext } from "../utils/context";
import CartTable from "./components/Cart/CartTable";
import { Products } from "./components";

const TripleCardList = () => {
  const [selectedUser, setSelectedUser] = useState<number>();
  const [selectedProduct, setSelectedProduct] = useState<string>();
  const [enableOpenDetail, setEnableOpenDetail] = useState(false);
  return (
    <SelectedContext.Provider
      value={{
        selectedUser,
        setSelectedUser,
        selectedProduct,
        setSelectedProduct,
        enableOpenDetail,
        setEnableOpenDetail,
      }}
    >
      <div className=" flex w-screen h-screen overflow-hidden">
        <div className="w-[50%] overflow-auto">
          <UserCardList />
        </div>
        <div className=" w-[50%]">
          <div className=" h-[50%] overflow-auto">
            <Products />
          </div>
          <div className=" h-[50%] ">
            <CartTable />
          </div>
        </div>
      </div>
    </SelectedContext.Provider>
  );
};

export default TripleCardList;
