import { useContext } from "react";
import { Detail, ProductList } from "./components";
import { SelectedContext } from "../../../utils/context";


const Products = () => {
  const { selectedProduct, enableOpenDetail } =
    useContext(SelectedContext);

  return <>
  {enableOpenDetail ? <Detail productId={selectedProduct.id} /> : <ProductList />}
  </>;
};
export default Products;
