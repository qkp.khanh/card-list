/* eslint-disable @typescript-eslint/no-explicit-any */
import { Input } from "antd";

type SearchProps = {
    value: string;
    disableSearching: boolean,
    onSearching: (params:any) => void,
}


const SearchBar = ({value, disableSearching, onSearching}:SearchProps) => {
    return (
        <Input
        value={value}
          allowClear
          disabled={disableSearching}
          placeholder="Input name"
          style={{ width: 304, marginLeft: "1rem" }}
          onChange={onSearching}
        />
        
    );
};

export default SearchBar;