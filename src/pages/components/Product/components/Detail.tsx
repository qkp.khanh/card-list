/* eslint-disable @typescript-eslint/no-explicit-any */
import { useContext, useEffect, useState } from "react";

import getProductDetail from "../../../../services/Products/getProductDetail";
import { Col, Image, Row } from "antd";
import { CloseIcon } from "../../../../components";
import { SelectedContext } from "../../../../utils/context";

const Detail = ({ productId }: { productId: string }) => {
  const { setSelectedProduct, setEnableOpenDetail } =
    useContext(SelectedContext);
  const [product, setProduct] = useState<any>();
  useEffect(() => {
    const fetchProductDetail = async () => {
      try {
        const { data, status } = await getProductDetail({
          id: productId,
        });
        console.log(data);
        if (status === 200) {
          setProduct(data);
        }
      } catch (error) {
        console.log(error);
      }
    };
    fetchProductDetail();
  }, [productId]);

  return (
    <div className="w-full h-full p-4 flex flex-col">
      <div className="mb-4">
        <CloseIcon
          onClick={() => {
            setSelectedProduct(undefined);
            setEnableOpenDetail(false);
          }}
        />
      </div>
      <Image.PreviewGroup>
        <div className="flex gap-2 max-w-full overflow-x-auto my-4">
          {product?.images.map((image: string, index: number) => (
            <div key={index}>
              <Image width={200} height={150} src={image} />
            </div>
          ))}
        </div>
      </Image.PreviewGroup>
      <div className="p-4 border rounded w-full mt-4">
        <Row>
          <Col span={8}>
            <div className="flex flex-col gap-2">
              <div>Category:</div>
              <div>Brand:</div>
              <div>Title:</div>
              <div>Price:</div>
              <div>Description:</div>
            </div>
          </Col>
          <Col span={8}>
            <div className="flex flex-col gap-2">
              <div>{product?.category}</div>
              <div>{product?.brand}</div>
              <div>{product?.title}</div>
              <div>{product?.price}</div>
              <div>{product?.description}</div>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
};
export default Detail;
