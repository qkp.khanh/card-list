/* eslint-disable @typescript-eslint/no-explicit-any */
const Card = ({
  info,
  onClickProduct,
}: {
  info: any;
  onClickProduct?: (info:any) => void;
}) => {
  return (
    <div
      className={`flex flex-col bg-gray-50 shadow-xl hover:shadow-2xl border-2 rounded-lg justify-center border-gray-50`}
      {...(onClickProduct && { onClick: () => onClickProduct(info) })}
    >
      <img src={info.thumbnail} className=" w-full h-[10rem] rounded-md" />
      <div className="px-4 pt-4">
        <span className="px-2 py-1 leading-none bg-orange-200 text-orange-800 rounded-full font-semibold uppercase tracking-wide text-xs">
          {info.category}
        </span>
        <h2 className="font-bold text-sm py-4 h-[5.5rem]">{info.title}</h2>
      </div>
    </div>
  );
};

export default Card;
