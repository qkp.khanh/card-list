/* eslint-disable @typescript-eslint/no-explicit-any */
import { useEffect, useMemo, useRef, useState } from "react";
import { Button } from "antd";
import Card from "./Card";
import SearchBar from "./SearchBar";
import { ErrorPage } from "../../../components";
import { fetchUsers, searchingUsersByName } from "../../../fetchers/Users";

const UserCardList = () => {
  const [list, setList] = useState<any>({});
  const { currentList: currList = [], searchList: searchList = [] } = list;
  const [total, setTotal] = useState<number>(0);
  const [doneFetchDataFirstRender, setDoneFetchDataFirstRender] =
    useState<boolean>(false);
  const [isSearching, setIsSearching] = useState<boolean>(false);
  const [error, setError] = useState<any>();

  const cachedData = useRef<any>({});
  const page = useRef<number>(1);
  const fetchDataAbortController = useRef<any>(null);
  const searchingController = useRef<any>(null);

  const maxPageMemorized = useMemo(() => total / 20, [total]);
  const nextPage = page.current + 1;
  const prevPage = page.current - 1;
  const disableFetchPrev = error || page.current === 1;
  const disableFetchNext = error || page.current === maxPageMemorized;
  const disableSearching = currList.length === 0;


  const handleOnCancellingSearching = () => {
    searchingController.current && searchingController.current.abort();
  };

  const onSearching = async (e: any) => {
    // cancelSearchingRequest();
    const searchName = e.target.value.trim();
    if (!searchName) {
      setIsSearching(false);
      return;
    }
    setIsSearching(true);
    handleOnCancellingSearching();
    try {
      searchingController.current = new AbortController();
      const searchResult = await searchingUsersByName({
        name: searchName,
        signalControl: searchingController.current.signal,
      });
      setList((prev: any) => ({ ...prev, searchList: searchResult }));
    } catch (e) {
      console.log(e);
    }
  };

  const fetchData = async (page: number) => {
    if (page in cachedData.current) return;
    try {
      fetchDataAbortController.current = new AbortController();
      const { listData, total } = await fetchUsers({
        page: page,
        signalControl: fetchDataAbortController.current.signal,
      });
      cachedData.current[page] = listData;
      if (currList.length === 0) {
        setTotal(total);
        setList((prev: any) => ({ ...prev, currentList: listData }));
        setDoneFetchDataFirstRender(true);
      }
    } catch (e: any) {
      console.log(e);
      setError(e.message);
    }
  };

  useEffect(() => {
    !doneFetchDataFirstRender && fetchData(page.current);
    doneFetchDataFirstRender && fetchData(nextPage);
  }, [doneFetchDataFirstRender]); // use "doneFetchDataFirstRender" to separate 2 API calls

  const handleOnCancellingFetchingData = () => {
    fetchDataAbortController.current && fetchDataAbortController.current.abort();
  }

  const handleOnClickNext = async () => {
    // handling spamming on slow 3G condition
    handleOnCancellingFetchingData();
    await fetchData(nextPage);
    page.current = nextPage;
    setList((prev: any) => ({
      ...prev,
      currentList: cachedData.current[nextPage],
    }));
    //prevent cache data if next page is max
    nextPage !== maxPageMemorized && (await fetchData(nextPage + 1));
  };

  const handleOnClickPrev = async () => {
    // handling spamming on slow 3G condition
    handleOnCancellingFetchingData();
    await fetchData(prevPage);
    setList((prev: any) => ({
      ...prev,
      currentList: cachedData.current[prevPage],
    }));
    page.current = prevPage;
  };

  return (
    <>
      <div className="flex pt-4 content-center justify-center">
        <Button disabled={disableFetchPrev} onClick={handleOnClickPrev}>
          Prev
        </Button>

        <Button disabled={disableFetchNext} onClick={handleOnClickNext}>
          Next
        </Button>
      </div>
      <div className="container mx-auto pt-4">
        <SearchBar
          disableSearching={disableSearching}
          onSearching={onSearching}
        />

        {error ? (
          <ErrorPage errorText={error} hiddenBtn={false} />
        ) : (
          <div className="flex flex-wrap justify-center items-center">
            {isSearching
              ? searchList.map((element: any, index: number) => (
                  <div
                    className="w-full p-4 sm:w-1/2 md:w-1/2 xl:w-1/4"
                    key={index}
                  >
                    <Card info={element} />
                  </div>
                ))
              : currList?.map((element: any, index: number) => (
                  <div
                    className="w-full p-4 sm:w-1/2 md:w-1/2 xl:w-1/4"
                    key={index}
                  >
                    <Card info={element} />
                  </div>
                ))}
          </div>
        )}
      </div>
    </>
  );
};

export default UserCardList;
