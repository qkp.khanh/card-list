/* eslint-disable @typescript-eslint/no-explicit-any */
import { Input } from "antd";

type SearchProps = {
    disableSearching: boolean,
    onSearching: (params:any) => void,
}


const SearchBar = ({disableSearching, onSearching}:SearchProps) => {
    return (
        <Input
          allowClear
          disabled={disableSearching}
          placeholder="Input name"
          style={{ width: 304, marginLeft: "1rem" }}
          onChange={onSearching}
        />
        
    );
};

export default SearchBar;