/* eslint-disable @typescript-eslint/no-explicit-any */
import { useContext } from "react";
import { SelectedContext } from "../../../utils/context";

const Card = ({ info }: {info: any}) => {
  const { selectedUser, setSelectedUser } = useContext(SelectedContext);

  return (
    <div
      className={`flex flex-col bg-gray-50 shadow-xl hover:shadow-2xl rounded-lg justify-center ${
        selectedUser === info.id
          ? "border-2 border-red-500"
          : "border-2 border-gray-50"
      }`}
      onClick={() => setSelectedUser(info.id)}
    >
      <img src={info.image} className=" w-full h-[10rem] rounded-md" />
      <div className="px-4 pt-4">
        <span className="px-2 py-1 leading-none bg-orange-200 text-orange-800 rounded-full font-semibold uppercase tracking-wide text-xs">
          {info.gender}
        </span>
        <p className="font-bold text-sm py-4 h-[5.5rem]">
          {info.firstName} {info.lastName}
        </p>
      </div>
    </div>
  );
};

export default Card;
