/* eslint-disable @typescript-eslint/no-explicit-any */
import { Table } from "antd";
import { useContext, useEffect, useRef, useState } from "react";
import { SelectedContext } from "../../../utils/context";
import { CartsFetcher } from "../../../fetchers/Carts";
import { ErrorPage, Spinner } from "../../../components";
const columns = ({
  onClickTitle,
}: {
  onClickTitle: (product: any) => void;
}) => [
  {
    key: "title",
    title: "Title",
    render: (record: any) => {
      return (
        <div
          className="text-blue-600 cursor-pointer"
          onClick={() => onClickTitle(record)}
        >
          {record.title}
        </div>
      );
    },
  },
  {
    key: "price",
    dataIndex: "price",
    title: "Price",
  },
  {
    key: "quantity",
    dataIndex: "quantity",
    title: "Quantity",
  },
  {
    key: "total",
    dataIndex: "total",
    title: "Total ($)",
  },
  {
    key: "discountPercentage",
    dataIndex: "discountPercentage",
    title: "Discount (%)",
  },
  {
    key: "discountedPrice",
    dataIndex: "discountedPrice",
    title: "Discounted Price ($)",
  },
];

const CartTable = () => {
  const { selectedUser, setSelectedProduct } = useContext(SelectedContext);
  const [cartData, setCartData] = useState<any>([]);
  const emptyCart = cartData.length === 0;
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState("");
  const abortController = useRef<any>(null);

  const fetchData = async () => {
    try {
      abortController.current = new AbortController();
      if (!selectedUser) return;
      setLoading(true);

      const res = await CartsFetcher({
        userId: selectedUser,
        signalControl: abortController.current.signal,
      });
      setCartData(res ? res.products : []);
    } catch (error: any) {
      console.log(error);
      setError(error.message);
    }
    setLoading(false);
  };

  useEffect(() => {
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    return () => {
      abortController.current && abortController.current.abort();
    };
  }, [selectedUser]);

  const handleOnClickTitle = (product: any) => {
    console.log("click product table", product);
    setSelectedProduct(product);
  };
  if (loading) return <Spinner />;
  if (error) return <ErrorPage errorText={error} hiddenBtn={true} />;
  return (
    <div className=" h-[95%] text-center mx-4 mt-2 flex items-center justify-center rounded-lg shadow-[0_2px_15px_-3px_rgba(0,0,0,0.07),0_10px_20px_-2px_rgba(0,0,0,0.04)]">
      {emptyCart ? (
        <div className=" text-5xl text-gray-400 tracking-wide">Empty Cart</div>
      ) : (
        <Table
          loading={loading}
          style={{ width: "100%", height: "100%" }}
          columns={columns({ onClickTitle: handleOnClickTitle })}
          dataSource={cartData}
          rowKey={(record) => record.id}
          scroll={{ y: 400 }}
          pagination={false}
        />
      )}
    </div>
  );
};

export default CartTable;
