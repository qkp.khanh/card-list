import axios from "axios";
import { REQUEST_TIMEOUT } from "../../constants";

const getProductDetail = async ({
  id,
}: {
  id: string;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
}) => {
  return await axios
    .get(`https://dummyjson.com/products/${id}`, {
      timeout: REQUEST_TIMEOUT,
    })
    .catch((e) => {
      throw e;
    });
};

export default getProductDetail;
