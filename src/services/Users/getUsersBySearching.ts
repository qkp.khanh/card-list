import axios from "axios";
import { REQUEST_TIMEOUT } from "../../constants";
import { errorHandler } from "../../utils/helper";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const getUsersBySearching = async({ name, signalControl }: { name: string, signalControl:any }) => {
  return await axios.get(
    `https://dummyjson.com/users/search?q=${name}`, {signal: signalControl,  timeout: REQUEST_TIMEOUT}
  ).catch((e) => {throw errorHandler(e)}) 
};

export default getUsersBySearching;