import axios from "axios";
import { REQUEST_TIMEOUT } from "../../constants";
import { errorHandler } from "../../utils/helper";

const PER_PAGE = 20
// eslint-disable-next-line @typescript-eslint/no-explicit-any
const getUserList = async({ page, limit = 20, signalControl}: { page: number; limit?: number, signalControl?:any }) => {
  return await axios.get(
    `https://dummyjson.com/users?skip=${(page - 1) * PER_PAGE}&limit=${limit}`, {signal: signalControl, timeout: REQUEST_TIMEOUT}
  ).catch((e) => {throw errorHandler(e)}) 
};

export default getUserList;