import axios from "axios"
import { REQUEST_TIMEOUT } from "../../constants";
// import { errorHandler } from "../../utils/helper";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const getCartByUser = async ({ userId, signalControl }: { userId: number, signalControl: any }) => {
    return await axios.get(`https://dummyjson.com/carts/user/${userId}`, { signal: signalControl, timeout: REQUEST_TIMEOUT }
    ).catch((e) => {throw (e)})
}

export default getCartByUser;